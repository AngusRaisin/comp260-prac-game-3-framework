﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletMove : MonoBehaviour {

	// separate speed and direction so we can 
	// tune the speed without changing the code
	public float speed = 10.0f;
	public Vector3 direction;
	private Rigidbody rigidbody;
	private float lifetime;

	void Start () {
		rigidbody = GetComponent<Rigidbody>();  
		lifetime = 0;
	}

	void FixedUpdate () {
		lifetime = lifetime + Time.deltaTime;
		rigidbody.velocity = speed * direction;
		if (lifetime >= 10)
			Destroy (gameObject);
	}

	void OnCollisionEnter(Collision collision) {
		// Destroy the bullet
		Destroy(gameObject);
	}

}
