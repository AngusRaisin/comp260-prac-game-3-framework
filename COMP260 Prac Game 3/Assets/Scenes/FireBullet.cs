﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireBullet : MonoBehaviour {

	public BulletMove bulletPrefab;
	private float fireCooldown;

	void Start () {
		fireCooldown = 1;
	}

	void Update () {
		// do not run if the game is paused
		if (Time.timeScale == 0) {
			return;
		}

		fireCooldown = fireCooldown + Time.deltaTime;
		// when the button is pushed, fire a bullet
		if (Input.GetButtonDown("Fire1") && fireCooldown >= 1) {
			BulletMove bullet = Instantiate (bulletPrefab);
			// the bullet starts at the player's position
			bullet.transform.position = transform.position;

			// create a ray towards the mouse location
			Ray ray = 
				Camera.main.ScreenPointToRay(Input.mousePosition);
			bullet.direction = ray.direction;
			fireCooldown = 0;
		}
	}

}
